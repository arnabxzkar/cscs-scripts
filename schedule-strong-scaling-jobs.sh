#!/usr/bin/env bash

source config.sh

module load daint-gpu
module use "$MODULEDIR"
module load CNTK


if 
  [[ -d Output ]] && [[ $1 != '-f' ]] 
then
  echo "Output dir still exists. Exiting."
  exit 1
fi

timestamp="$( date +%y%m%d%H%M )"

for i in 1 #2 4 8 16 32
do
  echo -n "Scheduling job for $i nodes: "
  sbatch -N $i run-strong.sbatch $i "$( pwd )" $timestamp
done
