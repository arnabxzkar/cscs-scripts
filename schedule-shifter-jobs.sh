#!/usr/bin/env bash

if 
  [[ -d Output ]] && [[ $1 != '-f' ]] 
then
  echo "Output dir still exists. Exiting."
  exit 1
fi

timestamp="$( date +%y%m%d%H%M )"

for i in 2
do
  echo -n "Scheduling job for $i nodes: "
  sbatch -N $i run-shifter.sbatch $i "$( pwd )" $timestamp
done
