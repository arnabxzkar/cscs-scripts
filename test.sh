testname="$1"
nnodes="$2"
dir="$3"

source /root/.bashrc 
cd "$dir"
source config.sh

bsize=$((nnodes*32))
esize=$((bsize*50))

cntk configFile="${testname}" esize=$esize bsize=$bsize nnodes=$nnodes datadir="$DATADIR"
