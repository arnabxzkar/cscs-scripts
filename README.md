# Benchmarking utilities for CNTK on Piz Daint supercomputer

This repository contains a collection of scripts and CNTK config files
suitable for running on the CSCS Piz Daint supercomputer.

## Prerequisites

CNTK has to be compiled and made available as a module. The easiest way
to achieve this is through the provided 
[EasyBuild framework](https://user.cscs.ch/scientific_computing/code_compilation/easybuild_framework/)
on Piz Daint.

An easyconfig file for CNTK is already available.

On top of that, the ImageNet dataset has to be prepared and made available,
ideally on the scratch space.

## Settings

Modify the file `config.sh`. It contains the following two settings:

* `DATADIR`: Directory containing the ImageNet dataset (If the dataset
  is fragmented, point this to the directory containing `train_map.txt`).
* `MODULEDIR`: Directory containing the EasyBuild module repository where
  CNTK can be found


## Usage

* To run weak scaling tests, execute `schedule_weak_scaling_jobs.sh`
* To run weak scaling tests in shifter containers, execute `schedule_shifter_jobs.sh`
* To run strong scaling tests, execute `schedule_strong_scaling_jobs.sh`

These scripts schedule jobs which run the experiments on up to 32 nodes. The node counts
on which to run the experiments can be adjusted in these files directly. The concrete
experiments to run can be adjusted in the files `run_weak.sbatch`, `run_shifter.sbatch`
and `run_strong.sbatch`, respectively.
